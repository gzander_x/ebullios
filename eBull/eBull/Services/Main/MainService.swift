//  Copyright © 2021 WinFox. All rights reserved.
//
import Alamofire
import SwiftyJSON

typealias RequestJsonCompletion = (_ results: JSON?, _ isConnection: Bool, _ error: Error?) -> Void
typealias OnlyErrorCompletion = (_ error: String?) -> Void

class MainService: NSObject {
    
    public struct APIManager {
        static let sharedManager: SessionManager = {
            let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 20
            return SessionManager(configuration: configuration)
        }()
    }
    
    
    // MARK: - Const
    let reachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.apple.com")
    
    internal let host = Constants.host
    
    // MARK: - Request
    public func sendRequest(requestType: HTTPMethod, url:String, parameters:[String: AnyObject]?, paramsEncoding: ParameterEncoding, headers: HTTPHeaders? = nil, completion:@escaping RequestJsonCompletion) {
        if let manager = Alamofire.NetworkReachabilityManager(host: "www.apple.com") {
            if !manager.isReachable {
                completion(nil, false, nil)
                NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "noConnect")))
                if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                    //appDelegate.showNoInternetScreen()
                }
                return
            }
        }
        
        APIManager.sharedManager.request(url as URLConvertible, method: requestType, parameters: parameters, encoding: paramsEncoding, headers: headers)
            //            //.authenticate(usingCredential: nil)
            .responseJSON { (dataResponse) in
                //                print(parameters as Any)
                //                debugPrint(dataResponse)
                switch dataResponse.result {
                case .success(let data):
                    if dataResponse.response?.statusCode == 402 {
                        //self.fetchAndSendReceipt()
                        return
                    }
                    let json = JSON(data)
                    completion(json, true, nil)
                case .failure(let error):
                    if dataResponse.response?.statusCode == 401 {
                        
                        //TODO: reftesh TOKEN
                        //                        TokenService.shared.refreshToken { (error) in
                        //
                        //                        }
                        
                    }
                    completion(nil, true, error)
                }
            }
    }
    
    //MARK: - Cancelation request
    public func sendCancelationRequest(requestType: HTTPMethod, url:String, params:[String: AnyObject]?, paramsEncoding: ParameterEncoding, headers: HTTPHeaders? = nil, completion:@escaping RequestJsonCompletion) {
        
        let concurrentQueue = DispatchQueue(label: "concurrentqueue", qos: .utility, attributes: .concurrent)
        
        if #available(iOS 9.0, *) {
            APIManager.sharedManager.session.getAllTasks(completionHandler: { (tasks) -> Void in
                tasks.forEach({
                    $0.cancel()
                })
            })
        }
        
        concurrentQueue.async {
            APIManager.sharedManager.request(url as URLConvertible, method: requestType, parameters: params, encoding: paramsEncoding, headers: headers).responseJSON(queue: concurrentQueue) { (dataResponse) in
                
                //debugPrint(dataResponse)
                switch dataResponse.result {
                case .success(let data):
                    if dataResponse.response?.statusCode == 402 {
                        //self.fetchAndSendReceipt()
                        return
                    }
                    let json = JSON(data)
                    completion(json, true, nil)
                case .failure(let error):
                    if dataResponse.response?.statusCode == 401 {
                        print(dataResponse.error?.localizedDescription as Any)
                    }
                    completion(nil, true, error)
                }
            }
        }
    }
    
    //MARK: - BackgroundQueue
    public func sendBackgroundQueueRequest(requestType: HTTPMethod, url:String, params:[String: AnyObject]?, paramsEncoding: ParameterEncoding, headers: HTTPHeaders? = nil, completion:@escaping RequestJsonCompletion) {
        
        let concurrentQueue = DispatchQueue(label: "concurrentqueue", qos: .utility, attributes: .concurrent)
        
        concurrentQueue.async {
            APIManager.sharedManager.request(url as URLConvertible, method: requestType, parameters: params, encoding: paramsEncoding, headers: headers).responseJSON(queue: concurrentQueue) { (dataResponse) in
                
                //debugPrint(dataResponse)
                switch dataResponse.result {
                case .success(let data):
                    if dataResponse.response?.statusCode == 402 {
                        //self.fetchAndSendReceipt()
                        return
                    }
                    let json = JSON(data)
                    completion(json, true, nil)
                case .failure(let error):
                    if dataResponse.response?.statusCode == 401 {
                        
                        //TODO: reftesh TOKEN
                        //                        TokenService.shared.refreshToken { (error) in
                        //
                        //                        }
                        
                    }
                    completion(nil, true, error)
                }
            }
        }
    }
}
struct Connectivity {
    static let sharedInstance = NetworkReachabilityManager()!
    static var isConnectedToInternet:Bool {
        return self.sharedInstance.isReachable
    }
}
