//  Copyright © 2020 WinFox. All rights reserved.
//
import UIKit
import Foundation

@IBDesignable public class GradientView: UIView {
    @IBInspectable public var leftColor: UIColor = UIColor.white {
        didSet {
            if let _ = gradientLayer.colors?.first {
                gradientLayer.colors![0] = leftColor.cgColor
            } else {
                gradientLayer.colors = [leftColor.cgColor, rightColor.cgColor]
            }
        }
    }
    @IBInspectable public var rightColor: UIColor = UIColor.black {
        didSet {
            if let _ = gradientLayer.colors?.last {
                gradientLayer.colors![gradientLayer.colors!.count - 1] = rightColor.cgColor
            } else {
                gradientLayer.colors = [leftColor.cgColor, rightColor.cgColor]
            }
        }
    }
    
    @IBInspectable public var startXPoint: CGFloat {
        get {
            return gradientLayer.startPoint.x
        }
        set {
            gradientLayer.startPoint.x = newValue
        }
    }
    
    @IBInspectable public var startYPoint: CGFloat {
        get {
            return gradientLayer.startPoint.y
        }
        set {
            gradientLayer.startPoint.y = newValue
        }
    }
    
    @IBInspectable public var endXPoint: CGFloat {
        get {
            return gradientLayer.endPoint.x
        }
        set {
            gradientLayer.endPoint.x = newValue
        }
    }
    
    @IBInspectable public var endYPoint: CGFloat {
        get {
            return gradientLayer.endPoint.y
        }
        set {
            gradientLayer.endPoint.y = newValue
        }
    }
    
    public init(leftColor: UIColor, rightColor: UIColor) {
        self.leftColor = leftColor
        self.rightColor = rightColor
        super.init(frame: CGRect.zero)
        createGradient()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createGradient()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        createGradient()
    }
    
    private func createGradient() {
        gradientLayer.colors = [leftColor.cgColor, rightColor.cgColor]
    }
    
    public var gradientLayer: CAGradientLayer {
        get {
            return self.layer as! CAGradientLayer
        }
    }
    
    override public class var layerClass: Swift.AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
}

extension CAGradientLayer {
    
    class func primaryGradient() -> UIImage? {
        let gradient = CAGradientLayer()
        gradient.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width
            , height: 124)
        gradient.colors = [UIColor.black.cgColor, UIColor.white.cgColor]
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 0, y: 0.5)
        return gradient.createGradientImage()
    }
   
    private func createGradientImage() -> UIImage? {
        var gradientImage: UIImage?
        UIGraphicsBeginImageContext(CGSize(width: UIScreen.main.bounds.width
            , height: 124))
        if let context = UIGraphicsGetCurrentContext() {
            render(in: context)
            gradientImage = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: UIEdgeInsets.zero, resizingMode: .stretch)
        }
        UIGraphicsEndImageContext()
        return gradientImage
    }
}
