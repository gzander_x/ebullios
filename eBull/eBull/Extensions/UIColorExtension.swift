//  Copyright © 2020 WinFox. All rights reserved.
//
import UIKit

extension UIColor {
    
    @nonobjc class var customOrange: UIColor {
        return UIColor.colorWithHexString(hex: "#E58244")
    }
    
    @nonobjc class var customBlue: UIColor {
        return UIColor.colorWithHexString(hex: "#3179BB")
    }
    
    @nonobjc class var mainDeepBlue: UIColor {
        return UIColor.colorWithHexString(hex: "#231D46")
    }
    
    @nonobjc class var mainMutedBlue: UIColor {
        return UIColor.colorWithHexString(hex: "#3E3862")
    }
    
    @nonobjc class var mainVividСyan: UIColor {
        return UIColor.colorWithHexString(hex: "#0ee5cc")
    }
    
    @nonobjc class var mainMainlyBlue: UIColor {
        return UIColor.colorWithHexString(hex: "#289bdb")
    }
    
    @nonobjc class var normalStatusLabelColor: UIColor {
        return UIColor(red: 78 / 255.0, green: 78.0 / 255.0, blue: 78.0 / 255.0, alpha: 0.5)
    }
    
    static func colorWithHexString (hex:String) -> UIColor {
        
        var cString = hex.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.count != 6) {
            return UIColor.gray
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    
}
