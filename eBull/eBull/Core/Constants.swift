//
//  Constants.swift
//  eBull
//
//  Created by Dmitry on 09.12.2021.
//

import UIKit

class Constants: NSObject {
    
    static let host = "..."
    static let hostImage = "..."
    static let locale = Locale.current.languageCode?.lowercased() ?? "en"
    static let smsConfirmationCodeLength = 6
    
    static let topSafeArea = UIApplication.shared.keyWindow?.safeAreaInsets.top
    
}
