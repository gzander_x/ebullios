//
//  UserModel.swift
//  Fat Fighter
//
//  Created by Dmitry on 07.12.2021.
//

import ObjectMapper

class UserModel: NSObject, NSCoding, Mappable {
    
    required public init(coder decoder: NSCoder) {
        first_name = decoder.decodeObject(forKey: "first_name") as? String
        last_name = decoder.decodeObject(forKey: "last_name") as? String
        phone = decoder.decodeObject(forKey: "phone") as? String
        email = decoder.decodeObject(forKey: "email") as? String
        photo = decoder.decodeObject(forKey: "photo") as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(self.first_name, forKey: "first_name")
        aCoder.encode(self.last_name, forKey: "last_name")
        aCoder.encode(self.phone, forKey: "phone")
        aCoder.encode(self.email, forKey: "email")
        aCoder.encode(self.photo, forKey: "photo")
    }
    
    var first_name: String?
    var last_name: String?
    var phone: String?
    var email: String?
    var photo: String?
    
    override init() {
        super.init()
    }
    
    required init?(map: Map) {
        super.init()
        self.mapping(map:map)
    }
    
    func mapping(map: Map) {
        first_name      <- map["first_name"]
        last_name       <- map["last_name"]
        phone           <- map["phone"]
        email           <- map["email"]
        photo           <- map["photo"]
    }
    
}

