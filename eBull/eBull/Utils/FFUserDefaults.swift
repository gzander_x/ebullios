//
//  FFUserDefaults.swift
//  Fat Fighter
//
//  Created by Dmitry on 07.12.2021.
//

import UIKit
import CoreLocation
class FFUserDefaults: NSObject {

    //MARK: - Token
    static func setToken (token: String?) {
        let defaults = UserDefaults.standard
        defaults.set(token, forKey: "token")
        defaults.synchronize()
    }
    
    static func getToken() -> String? {
        return UserDefaults.standard.string(forKey: "token")
    }
    
    //MARK: - Refresh Token
    static func setRefreshToken (refreshToken: String?) {
        let defaults = UserDefaults.standard
        defaults.set(refreshToken, forKey: "refreshToken")
        defaults.synchronize()
    }
    
    static func getRefreshToken() -> String? {
        return UserDefaults.standard.string(forKey: "refreshToken")
    }
    
    //MARK: - UserLocation
    public static func setUserLocation(location: CLLocationCoordinate2D) {
        let defaults = UserDefaults.standard
        defaults.set(Double(location.latitude), forKey: "USER_LAT")
        defaults.set(Double(location.longitude), forKey: "USER_LON")
        defaults.synchronize()
    }
    
    public static func getUserLon() -> Double? {
        return UserDefaults.standard.double(forKey: "USER_LON")
    }
    public static func getUserLat() -> Double? {
        return UserDefaults.standard.double(forKey: "USER_LAT")
    }
    
    //MARK: - First run state
    static func SetIsFirstRun (isFirstRun: Bool) {
        let defaults = UserDefaults.standard
        defaults.set(isFirstRun, forKey: "isFirstRun")
        defaults.synchronize()
    }
    
    static func GetIsFirstRun() -> Bool {
        return UserDefaults.standard.bool(forKey: "isFirstRun")
    }
}
