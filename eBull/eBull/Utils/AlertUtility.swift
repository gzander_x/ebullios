//  Copyright © 2021 WinFox. All rights reserved.
//
import UIKit

public class AlertUtility: NSObject {
    
    static func showAlert(message: String, title: String?, fromVC: UIViewController) {
        var alertController: UIAlertController!
        if title != nil {
            alertController = UIAlertController(title: title!, message:
                                                    message, preferredStyle: UIAlertController.Style.alert)
        } else {
            alertController = UIAlertController(title: nil, message:
                                                    message, preferredStyle: UIAlertController.Style.alert)
        }
        let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        cancelAction.setValue(UIColor.black, forKey: "titleTextColor")
        alertController.addAction(cancelAction)
        fromVC.present(alertController, animated: true, completion: nil)
    }
    
    static func showAlertWithReturn(message: String, title: String?, fromVC: UIViewController) {
        var alertController: UIAlertController!
        if title != nil {
            alertController = UIAlertController(title: title!, message:
                                                    message, preferredStyle: UIAlertController.Style.alert)
        } else {
            alertController = UIAlertController(title: nil, message:
                                                    message, preferredStyle: UIAlertController.Style.alert)
        }
        let cancelAction = UIAlertAction(title: "OK", style: .cancel) { _ in 
            fromVC.returnBack()
        }
        cancelAction.setValue(UIColor.black, forKey: "titleTextColor")
        alertController.addAction(cancelAction)
        fromVC.present(alertController, animated: true, completion: nil)
    }
    
}
