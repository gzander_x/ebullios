//
//  MainRouting.swift
//  eBull
//
//  Created by Dmitry on 09.12.2021.
//

import UIKit

class MainRouting: NSObject {
    
    static func showLocatorTabBar() {
        let myRootviewController = UIApplication.shared.keyWindow!.rootViewController as? MainTabBarVC
        myRootviewController?.selectedIndex = 0
    }
    static func showExpensesTabBar() {
        let myRootviewController = UIApplication.shared.keyWindow!.rootViewController as? MainTabBarVC
        myRootviewController?.selectedIndex = 1
    }
    static func showPaymentTabBar() {
        let myRootviewController = UIApplication.shared.keyWindow!.rootViewController as? MainTabBarVC
        myRootviewController?.selectedIndex = 2
    }
    static func showLimitTabBar() {
        let myRootviewController = UIApplication.shared.keyWindow!.rootViewController as? MainTabBarVC
        myRootviewController?.selectedIndex = 3
    }
    static func showOtherTabBar() {
        let myRootviewController = UIApplication.shared.keyWindow!.rootViewController as? MainTabBarVC
        myRootviewController?.selectedIndex = 4
    }
    
    static func showAuth() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.showAuthScreen()
        }
    }
    
    static func showMain() {
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.showMainScreen()
        }
    }
}
