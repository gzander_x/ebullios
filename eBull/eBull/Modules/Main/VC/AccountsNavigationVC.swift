//
//  AccountsNavigationVC.swift
//  eBull
//
//  Created by Dmitry on 09.12.2021.
//

import UIKit

class AccountsNavigationVC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigation()
    }
    
    private func navigation() {
        let vc = UIStoryboard(name: "Accounts", bundle: nil).instantiateViewController(withIdentifier: "AccountsVC") as! AccountsVC
        self.setViewControllers([vc], animated: false)
    }

}
