//
//  SettingsNavigationVC.swift
//  eBull
//
//  Created by Dmitry on 09.12.2021.
//

import UIKit

class SettingsNavigationVC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigation()
    }
    
    private func navigation() {
        let vc = UIStoryboard(name: "Settings", bundle: nil).instantiateViewController(withIdentifier: "SettingsVC") as! SettingsVC
        self.setViewControllers([vc], animated: false)
    }

}
