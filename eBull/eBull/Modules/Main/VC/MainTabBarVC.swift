//
//  MainTabBarVC.swift
//  Fat Fighter
//
//  Created by Dmitry on 07.12.2021.
//

import UIKit

class MainTabBarVC: UITabBarController {
    
    var numPages: Int {
        get{
            return Int(tabBar.frame.size.height)
        }
    }
   
    let borderView: UIView = {
        let view = UIView()
        view.backgroundColor = .mainMutedBlue
        view.contentMode = .center
        view.frame = CGRect(x: 0, y: -10, width: Int(UIScreen.main.bounds.width), height: 2)
        return view
    }()
    
    let backView: UIView = {
        let view = UIView()
        view.backgroundColor = .mainDeepBlue
        view.contentMode = .center
        view.frame = CGRect(x: 0, y: -8, width: Int(UIScreen.main.bounds.width), height: 8)
        return view
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedIndex = 0
        settingsTabBar()
        self.viewControllers?[0].view.layoutSubviews()
    }
    
    private func settingsTabBar() {
        self.tabBar.tintColor = .white
        self.tabBar.barTintColor = .mainDeepBlue
        self.tabBar.unselectedItemTintColor = .white.withAlphaComponent(0.5)
        self.tabBar.addSubview(backView)
        self.tabBar.addSubview(borderView)
    }
    
}
extension UITabBarController {
    
    func set(visible: Bool, animated: Bool, completion: ((Bool)->Void)? = nil ) {
        
        guard isVisible() != visible else {
            completion?(true)
            return
        }
        
        let offsetY = tabBar.frame.size.height
        let duration = (animated ? 0.3 : 0.0)
        
        let beginTransform:CGAffineTransform
        let endTransform:CGAffineTransform
        
        if visible {
            beginTransform = CGAffineTransform(translationX: 0, y: offsetY)
            endTransform = CGAffineTransform.identity
        } else {
            beginTransform = CGAffineTransform.identity
            endTransform = CGAffineTransform(translationX: 0, y: offsetY)
        }
        
        tabBar.transform = beginTransform
        if visible {
            tabBar.isHidden = false
        }
        
        UIView.animate(withDuration: duration, animations: {
            self.tabBar.transform = endTransform
        }, completion: { compete in
            completion?(compete)
            if !visible {
                self.tabBar.isHidden = true
            }
        })
    }
    
    func isVisible() -> Bool {
        return !tabBar.isHidden
    }
    
    func isNotVisible() -> Bool {
        return tabBar.isHidden
    }
}
extension UITabBarController{
    func getHeight()->CGFloat {
        return self.tabBar.frame.size.height
    }
    
    func getWidth()->CGFloat {
        return self.tabBar.frame.size.width
    }
}
