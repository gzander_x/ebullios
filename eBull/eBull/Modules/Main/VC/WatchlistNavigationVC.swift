//
//  WatchlistNavigationVC.swift
//  eBull
//
//  Created by Dmitry on 09.12.2021.
//

import UIKit

class WatchlistNavigationVC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigation()
    }
    
    private func navigation() {
        let vc = UIStoryboard(name: "Watchlist", bundle: nil).instantiateViewController(withIdentifier: "WatchlistVC") as! WatchlistVC
        self.setViewControllers([vc], animated: false)
    }

}
