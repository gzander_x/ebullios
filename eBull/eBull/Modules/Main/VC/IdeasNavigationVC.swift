//
//  IdeasNavigationVC.swift
//  eBull
//
//  Created by Dmitry on 09.12.2021.
//

import UIKit

class IdeasNavigationVC: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigation()
    }
    
    private func navigation() {
        let vc = UIStoryboard(name: "Ideas", bundle: nil).instantiateViewController(withIdentifier: "IdeasVC") as! IdeasVC
        self.setViewControllers([vc], animated: false)
    }

}
