//
//  TradesNavigationVC.swift
//  eBull
//
//  Created by Dmitry on 09.12.2021.
//

import UIKit

class TradesNavigationVC: UINavigationController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigation()
    }
    
    private func navigation() {
        let vc = UIStoryboard(name: "Trades", bundle: nil).instantiateViewController(withIdentifier: "TradesVC") as! TradesVC
        self.setViewControllers([vc], animated: false)
    }
    
    
}
