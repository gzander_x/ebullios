import UIKit

class CustomNavigationViewController: UINavigationController {
    override func viewDidLoad() {
        super.viewDidLoad()
        guard let image = CAGradientLayer.primaryGradient() else {
            self.navigationBar.barTintColor = UIColor.white
            return
        }
        self.navigationBar.barTintColor = UIColor(patternImage: image)
    }
}
