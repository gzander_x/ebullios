//
//  AppDelegate.swift
//  eBull
//
//  Created by Dmitry on 09.12.2021.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseCore

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        //FirebaseApp.configure() wait GoogleService-Info.plist
        
        self.showMainScreen() //main navigation
        
        
        return true
    }
    
    //MARK: - Navigation
    func showMainScreen() {
        let destvc = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()!
        self.setRootViewController(vc: destvc)
    }
    
    func showAuthScreen() {
        let destvc = UIStoryboard(name: "Login", bundle: nil).instantiateInitialViewController()!
        self.setRootViewController(vc: destvc)
    }
    
    func setRootViewController(vc: UIViewController) {
        if UIApplication.shared.keyWindow?.rootViewController == nil {
            self.window?.rootViewController = vc
            self.window?.makeKeyAndVisible()
        } else {
            UIApplication.shared.keyWindow?.set(rootViewController: vc)
        }
    }
    
    //MARK: - Lifecycle
    func applicationWillResignActive(_ application: UIApplication) {
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
    }
    
}
extension UIWindow {
    func set(rootViewController newRootViewController: UIViewController, withTransition transition: CATransition? = nil) {
        
        let previousViewController = rootViewController
        
        if let transition = transition {
            
            layer.add(transition, forKey: kCATransition)
        }
        
        rootViewController = newRootViewController
        
        // Update status bar appearance using the new view controllers appearance - animate if needed
        if UIView.areAnimationsEnabled {
            UIView.animate(withDuration: CATransaction.animationDuration()) {
                newRootViewController.setNeedsStatusBarAppearanceUpdate()
            }
        } else {
            newRootViewController.setNeedsStatusBarAppearanceUpdate()
        }
        
        /// The presenting view controllers view doesn't get removed from the window as its currently transistioning and presenting a view controller
        if #available(iOS 13.0, *) {
        } else {
            if let transitionViewClass = NSClassFromString("UITransitionView") {
                for subview in subviews where subview.isKind(of: transitionViewClass) {
                    subview.removeFromSuperview()
                }
            }
        }
        
        if let previousViewController = previousViewController {
            // Allow the view controller to be deallocated
            previousViewController.dismiss(animated: false) {
                // Remove the root view in case its still showing
                previousViewController.view.removeFromSuperview()
            }
        }
    }
}
